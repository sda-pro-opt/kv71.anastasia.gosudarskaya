#ifndef AMK_H_INCLUDED
#define AMK_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct AMK
{
    char *addr;
    char *oper_code;
    int AT;
    int AF;
};
void print_AMK(int, struct AMK*);
int fill_amk(struct AMK**);
#endif // AMK_H_INCLUDED
