#include "tree.h"
#include "AMK.h"
#include "token.h"
#include "header_for_lexer.h"

#define file_in "tests/test09/input.sig"
#define file_out "tests/test09/expected.txt"

void parser(token *tokens, struct AMK* table_amk, int amount_amk);
int signal_program(token *tok, struct node **p, struct AMK* table_amk, int amount_amk);
int program(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int procedure_identifier(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int constant_identifier(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int identifier(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int block(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int statements_list(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int declarations(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int constant_declarations(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int constant_declarations_list(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int constant_declaration(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int constanta(token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int choose_func_for_call(char *oper_code, token *tok, struct node *p, struct AMK* table_amk, int amount_amk);
int find_row_in_amk_po_str(char *str, struct AMK* table_amk, int amount_amk);
int find_amount_row(char *str, struct AMK* table_amk, int amount_amk);
int check_AT_AF(int i_count, int j, token *tok, struct node *p2, struct AMK* table_amk, int amount_amk);
int main()
{
    //lexer
    struct map keywords[keywords_size], delim[delim_size], *ident=NULL, *constant=NULL;

    int *SymbolCategories = fill_attr(SymbolCategories);
    fill_tables(keywords, delim);
    int count_ident=0, count_constant=0;

    lexer(file_in, SymbolCategories, &count_ident, &count_constant, keywords, delim, &ident, &constant);
    free(SymbolCategories);
    print_tables(count_ident, count_constant, keywords, delim, ident, constant);
    //parser
    struct AMK *table_amk=NULL;
    int amount_amk=fill_amk(&table_amk);
    print_AMK(amount_amk, table_amk);

    token *tokens=NULL;
    tokens = fill_token(tokens);
    parser(tokens, table_amk, amount_amk);
    return 0;
}

int current=0;
void parser(token *tokens, struct AMK* table_amk, int amount_amk)
{
    FILE *f=fopen(file_out , "wt");
    fclose(f);
    struct node *root = NULL;
    signal_program(tokens, &root, table_amk, amount_amk);
    //treeprint(root, 1);
    f=fopen(file_out , "at");
    print_tree_in_file(root, 1, f);
    fclose(f);
    treefree(root);
}

int signal_program(token *tok, struct node **p, struct AMK* table_amk, int amount_amk)
{
    *p=add_node("signal-program", *p);
    if (choose_func_for_call(table_amk[0].oper_code, tok, *p, table_amk, amount_amk) == 1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
int program(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("program",p);
    int j=find_row_in_amk_po_str("program", table_amk, amount_amk);
    return check_AT_AF(find_amount_row("program", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
}
int procedure_identifier(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("procedure-identifier",p);
    int j=find_row_in_amk_po_str("procedure-identifier", table_amk, amount_amk);
    int i_count=find_amount_row("procedure-identifier", table_amk, amount_amk);
    return check_AT_AF(i_count, j, tok, p2, table_amk, amount_amk);
}

int identifier(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("identifier",p);
    int j=find_row_in_amk_po_str("identifier", table_amk, amount_amk);
    int i_count=find_amount_row("identifier", table_amk, amount_amk);
    return check_AT_AF(i_count, j, tok, p2, table_amk, amount_amk);
}
int constant_identifier(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("constant-identifier",p);
    int j=find_row_in_amk_po_str("constant-identifier", table_amk, amount_amk);
    int i_count=find_amount_row("constant-identifier", table_amk, amount_amk);
    return check_AT_AF(i_count, j, tok, p2, table_amk, amount_amk);
}
int block(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("block",p);
    int j=find_row_in_amk_po_str("block", table_amk, amount_amk);
    return check_AT_AF(find_amount_row("block", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
}

int statements_list(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("statements-list",p);
    int j=find_row_in_amk_po_str("statements-list", table_amk, amount_amk);
    if (j==-1)
    {
        struct node *p3=NULL;
        p3=add_node("empty",p2);
        return 1;
    }
    return check_AT_AF(find_amount_row("statements-list", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
}

int declarations(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("declarations",p);
    int j=find_row_in_amk_po_str("declarations", table_amk, amount_amk);
    return check_AT_AF(find_amount_row("declarations", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
}
int constant_declarations(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("constant-declarations",p);
    if ((count_token(tok) != current) && (strcmp(find_str_po_lex_code(tok[current].lex_code), "CONST")==0))
    {
        int j=find_row_in_amk_po_str("constant-declarations", table_amk, amount_amk);
        return check_AT_AF(find_amount_row("constant-declarations", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
    }
    else
    {
        struct node *p3=NULL;
        p3=add_node("empty",p2);
        return 1;
    }
}
int constant_declarations_list(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("constant-declarations-list",p);
    if (tok[current].lex_code<700 && tok[current].lex_code>600)
    {
        int j=find_row_in_amk_po_str("constant-declarations-list", table_amk, amount_amk);
        return check_AT_AF(find_amount_row("constant-declarations-list", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
    }
    else
    {
        struct node *p3=NULL;
        p3=add_node("empty",p2);
        return 1;
    }
}
int constant_declaration(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("constant-declaration",p);
    int j=find_row_in_amk_po_str("constant-declaration", table_amk, amount_amk);
    return check_AT_AF(find_amount_row("constant-declaration", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
}
int constanta(token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    struct node *p2=NULL;
    p2=add_node("constant",p);
    int j=find_row_in_amk_po_str("constant", table_amk, amount_amk);
    return check_AT_AF(find_amount_row("constant", table_amk, amount_amk), j, tok, p2, table_amk, amount_amk);
}
int choose_func_for_call(char *oper_code, token *tok, struct node *p, struct AMK* table_amk, int amount_amk)
{
    int flag=0;
    if (strcmp(oper_code, "<signal-program>")==0)
        flag = signal_program(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<program>")==0)
        flag = program(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<procedure-identifier>")==0)
        flag = procedure_identifier(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<block>")==0)
        flag = block(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<statements-list>")==0)
        flag = statements_list(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<declarations>")==0)
        flag = declarations(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<constant-declarations>")==0)
        flag = constant_declarations(tok, p,table_amk, amount_amk);
    if (strcmp(oper_code, "<constant-declarations-list>")==0)
        flag = constant_declarations_list(tok, p,table_amk, amount_amk);
    if (strcmp(oper_code, "<constant-declaration>")==0)
        flag = constant_declaration(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<constant>")==0)
        flag = constanta(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<constant-identifier>")==0)
        flag = constant_identifier(tok, p, table_amk, amount_amk);
    if (strcmp(oper_code, "<identifier>")==0)
        flag = identifier(tok, p, table_amk, amount_amk);

    return flag;
}

int find_row_in_amk_po_str(char *str, struct AMK* table_amk, int amount_amk)
{
    for (int i=0; i<amount_amk; i++)
    {
        if (strcmp(table_amk[i].addr, str)==0)
        {
            return i;
        }
    }
    return -1;
}
int find_amount_row(char *str, struct AMK* table_amk, int amount_amk)
{
    int count=0;
    for (int i=0; i<amount_amk; i++)
    {
        if (strcmp(table_amk[i].addr, str)==0)
        {
            count++;
        }
    }
    return count;
}
int check_AT_AF(int i_count, int j, token *tok, struct node *p2, struct AMK* table_amk, int amount_amk)
{
    FILE *f=fopen(file_out , "at");
    if (count_token(tok)==0)
    {
        printf(f, "Parser:Error(line 1, column 1): Expected %s\n", p2->inf);
        fclose(f);
        return 0;
    }
    if (j==-1 && count_token(tok) != current)
    {
        fprintf(f, "Parser:Error(line %d, column %d): Expected %s\n",tok[current].row, tok[current].column, p2->inf);
        fclose(f);
        return 0;
    }
    for (int i=0; i<i_count; i++)
    {
        if (table_amk[j+i].oper_code[0]=='<')
        {
            if (choose_func_for_call(table_amk[j+i].oper_code, tok, p2, table_amk, amount_amk) == 1)
            {
                if (table_amk[j+i].AT==1)
                {
                    return 1;
                }
                else
                {
                    continue;
                }
            }
            else
            {
                if (table_amk[j+i].AF==1)
                {
                    return 0;
                }
                else
                {
                    continue;
                }
            }
        }
        else
        {
            if (count_token(tok) == current)
            {
                f=fopen(file_out , "at");
                fprintf(f, "Parser:Error(line %d, column %d): Expected %s\n",tok[current-1].row, tok[current-1].column, table_amk[j+i].oper_code);
                fclose(f);
                return 0;
            }
            if (strcmp(find_str_po_lex_code(tok[current].lex_code), table_amk[j+i].oper_code)==0)
            {
                struct node *p3=NULL;
                p3=add_node(find_str_po_lex_code(tok[current].lex_code), p2);
                if (table_amk[j+i].AT==1)
                {
                    current++;
                    return 1;
                }
                else
                {
                    current++;
                    continue;
                }
            }
            else
            {
                if (table_amk[j+i].AF==1)
                {
                    f=fopen(file_out , "at");
                    if (strcmp(table_amk[j+i].addr, "identifier")==0 || strcmp(table_amk[j+i].addr, "constant")==0)
                    {
                        fprintf(f, "Parser:Error(line %d, column %d): Expected %s\n",tok[current].row, tok[current].column, p2->inf);
                    }
                    else
                    {
                        fprintf(f,"Parser:Error(line %d, column %d): Expected %s\n",tok[current].row, tok[current].column, table_amk[j+i].oper_code);
                    }
                    fclose(f);
                    return 0;
                }
                else
                {
                    continue;
                }
            }
        }
    }
    f=fopen(file_out , "at");
    fprintf(f, "Parser:Error(line %d, column %d): Expected %s\n",tok[current-1].row, tok[current-1].column + strlen(find_str_po_lex_code(tok[current-1].lex_code)), p2->inf);
    fclose(f);
}
